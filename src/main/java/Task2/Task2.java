package Task2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Task2 {

    public static void main(String str[]) {
        List<Person> personList = new ArrayList<Person>();
        personList.add(new Person("First", 20));
        personList.add(new Person("Second", 21));
        personList.add(new Person("Third", 22));
        personList.add(new Person("Fourth", 23));
        personList.add(new Person("Fifth", 24));
        personList.add(new Person("Sixth", 25));
        Comparator<Person> nameComparator = Comparator.comparing(Person::getName);
        Comparator<Person> ageComparator = Comparator.comparing(Person::getAge);

        Collections.sort(personList, nameComparator);
        Collections.sort(personList, ageComparator);
        printPersonList(personList);
    }

    static void printPersonList(List<Person> personList) {
        personList.forEach(person -> System.out.println("Person Name = " + person.getName() + ", Age = " + person.getAge()));
    }

}
