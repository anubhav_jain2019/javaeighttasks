package Task3;

import Task2.Person;

import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.DoubleSupplier;
import java.util.function.Predicate;
import java.util.function.Supplier;

public interface Sandbox {

    Supplier<Person> supplier = () -> {
        return new Person("Varun", 30);
    };

    BiConsumer<Map<Integer, String>, String> biCon = (Map<Integer, String> map, String mapName) -> {
        map.forEach((key, val)->System.out.println(key+" "+val));
    };

    Predicate<Integer> greaterThanTen = (i) -> i > 10;

    Predicate<Integer> lowerThanTwenty = (i) -> i < 20;

    DoubleSupplier randomValue = () -> Math.random();

    static void printElements(List<Object> list) {
        list.forEach(System.out::println);
    }

    default void printObject(Object obj) {
        System.out.println(obj);
    }


}
