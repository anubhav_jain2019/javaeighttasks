package Task3;

import Task2.Person;

import java.util.HashMap;
import java.util.Map;

public class Task3 implements Sandbox {
    public static void main(String[] args) {
        Sandbox sb = new Task3();
        Person p = supplier.get();
        sb.printObject(p);

        System.out.println(randomValue.getAsDouble());

        Map<Integer, String> map = new HashMap<Integer,String>();
        map.put(100, "Mohan");
        map.put(110, "Sujeet");
        map.put(115, "Tom");
        map.put(120, "Danish");
        biCon.accept(map, "Student");

        System.out.println(greaterThanTen.and(lowerThanTwenty).test(15));
        System.out.println(greaterThanTen.and(lowerThanTwenty).negate().test(15));
    }
}