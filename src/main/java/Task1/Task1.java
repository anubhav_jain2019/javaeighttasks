package Task1;
public class Task1 {
    public static void main(String[] args) {
        Runnable runnable1 = () -> {
            for(int i=0;i<5;i++)
                System.out.println("Thread name : " + Thread.currentThread().getName()+" value = "+i);
        };
        Thread thread1 = new Thread(runnable1);

        Runnable runnable2 = () -> {
            for(int i=5;i<10;i++)
                System.out.println("Thread name : " + Thread.currentThread().getName()+" value = "+i);
        };
        Thread thread2 = new Thread(runnable2);

        thread1.start();
        thread2.start();
    }
}