package Task7;

import org.junit.Assert;
import org.junit.Test;

import java.util.function.*;
import java.util.*;
import java.util.stream.Collector;


public class IntCollectorTest {

    IntCollector intCollector =new IntCollector();

    @Test
    public void supplierTest() {
        Supplier<List<Integer>> supplier = intCollector.supplier();
        Assert.assertNotNull(supplier);
        Assert.assertEquals(supplier.get().size(),0);
    }

    @Test
    public void accumulatorTest() {
        BiConsumer<List<Integer>, Integer> accumulator = intCollector.accumulator();
        List<Integer> list = new ArrayList<Integer>();
        accumulator.accept(list,123);
        Assert.assertNotNull(accumulator);
        Assert.assertEquals(list.size(),1);
        Assert.assertEquals(list.get(0),(Integer) 123);
    }

    @Test
    public void combinerTest() {
        BinaryOperator<List<Integer>> combine = intCollector.combiner();

        List<Integer> list = new ArrayList<Integer>();
        list.add(123);

        List<Integer> list2 = new ArrayList<Integer>();
        list2.add(456);

        combine.apply(list, list2);

        Assert.assertNotNull(list);
        Assert.assertNotNull(list2);
        Assert.assertEquals(list2.size(),1);
        Assert.assertEquals(list.size(),2);
    }


    @Test
    public void finisherTest() {
        Function<List<Integer>, List<Integer>> finisher = intCollector.finisher();

        List<Integer> list = new ArrayList<Integer>();
        list.add(123);

        finisher.apply(list);

        Assert.assertNotNull(list);
        Assert.assertEquals(list.size(),1);
        Assert.assertEquals(list.get(0), (Integer) 123);
    }

    @Test
    public void characteristicsTest() {
        Set<Collector.Characteristics> characteristics = intCollector.characteristics();
        Assert.assertNotNull(characteristics);
        Assert.assertEquals(characteristics.size(),1);
        Assert.assertEquals(characteristics.contains(Collector.Characteristics.CONCURRENT),true);
    }

    @Test
    public void minIntTest() {
        Collector<Integer, List<Integer>, List<Integer>> minCollector = IntCollector.minInt(ArrayList::new);
        List<Integer> numbers = IntCollector.getIntStreamList();
        List<Integer> minList = numbers.stream().collect(minCollector);
        Assert.assertNotNull(minCollector);
        Assert.assertNotNull(minList);
        Assert.assertEquals(minList.size(),1);
        Assert.assertEquals(minList.get(0),(Integer)0);
    }

    @Test
    public void minIntParallelTest() {
        Collector<Integer, List<Integer>, List<Integer>> minCollector = IntCollector.minInt(ArrayList::new);
        List<Integer> numbers = IntCollector.getIntStreamList();
        List<Integer> minList = numbers.parallelStream().collect(minCollector);
        Assert.assertNotNull(minCollector);
        Assert.assertNotNull(minList);
        Assert.assertEquals(minList.size(),1);
        Assert.assertEquals(minList.get(0),(Integer)0);
    }

    @Test
    public void maxIntTest() {
        Collector<Integer, List<Integer>, List<Integer>> maxCollector = IntCollector.maxInt(ArrayList::new);
        List<Integer> numbers = IntCollector.getIntStreamList();
        List<Integer> maxList = numbers.stream().collect(maxCollector);
        Assert.assertNotNull(maxCollector);
        Assert.assertNotNull(maxList);
        Assert.assertEquals(maxList.size(),1);
        Assert.assertEquals(maxList.get(0),(Integer)9999990);
    }

    @Test
    public void maxIntParallelTest() {
        Collector<Integer, List<Integer>, List<Integer>> maxCollector = IntCollector.maxInt(ArrayList::new);
        List<Integer> numbers = IntCollector.getIntStreamList();
        List<Integer> maxList = numbers.parallelStream().collect(maxCollector);
        Assert.assertNotNull(maxCollector);
        Assert.assertNotNull(maxList);
        Assert.assertEquals(maxList.size(),1);
        Assert.assertEquals(maxList.get(0),(Integer)9999990);
    }

    @Test
    public void summingIntTest() {
        Collector<Integer, List<Integer>, List<Integer>> summingIntCollector = IntCollector.summingInt(ArrayList::new);
        List<Integer> numbers = IntCollector.getIntStreamList();
        List<Integer> summingList = numbers.stream().collect(summingIntCollector);
        Assert.assertNotNull(summingIntCollector);
        Assert.assertNotNull(summingList);
        Assert.assertEquals(summingList.size(),1);
        Assert.assertEquals(summingList.get(0),(Integer)653067456);
    }

    @Test
    public void summingIntParallelTest() {
        Collector<Integer, List<Integer>, List<Integer>> summingIntCollector = IntCollector.summingInt(ArrayList::new);
        List<Integer> numbers = IntCollector.getIntStreamList();
        List<Integer> summingList = numbers.parallelStream().collect(summingIntCollector);
        Assert.assertNotNull(summingIntCollector);
        Assert.assertNotNull(summingList);
        Assert.assertEquals(summingList.size(),1);
        Assert.assertEquals(summingList.get(0),(Integer)653067456);
    }

    @Test
    public void countIntTest() {
        Collector<Integer, List<Integer>, List<Integer>> countIntCollector = IntCollector.countInt(ArrayList::new);
        List<Integer> numbers = IntCollector.getIntStreamList();
        List<Integer> countList = numbers.stream().collect(countIntCollector);
        Assert.assertNotNull(countIntCollector);
        Assert.assertNotNull(countList);
        Assert.assertEquals(countList.size(),1);
        Assert.assertEquals(countList.get(0),(Integer)1000000);
    }


    @Test
    public void countIntParallelTest() {
        Collector<Integer, List<Integer>, List<Integer>> countIntCollector = IntCollector.countInt(ArrayList::new);
        List<Integer> numbers = IntCollector.getIntStreamList();
        List<Integer> countList = numbers.parallelStream().collect(countIntCollector);
        Assert.assertNotNull(countIntCollector);
        Assert.assertNotNull(countList);
        Assert.assertEquals(countList.size(),1);
        Assert.assertEquals(countList.get(0),(Integer)1000000);
    }
}