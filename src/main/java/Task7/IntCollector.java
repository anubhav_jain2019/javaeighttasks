package Task7;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class IntCollector implements Collector<Integer, List<Integer>, List<Integer>> {

    public static List<Integer> getIntStreamList() {
        return Stream.iterate(0, i -> i + 10).limit(1000000).collect(Collectors.toList());
    }

    public static Collector<Integer, List<Integer>, List<Integer>>
    summingInt(final Supplier<List<Integer>> supplier) {
        return Collector.of(
                supplier,
                (a, t) -> {
                    long val = a.size() > 0 ? a.get(0) + t : t;
                    if (a.size() > 0) {
                        a.set(0, (int) val);
                    } else {
                        a.add((int) val);
                    }
                },
                (a, b) -> {
                    long bval = b.get(0);
                    long aval = a.get(0) + bval;
                    a.set(0, (int) aval);
                    return a;
                },
                a -> a, Collector.Characteristics.CONCURRENT);
    }

    public static Collector<Integer, List<Integer>, List<Integer>>
    minInt(final Supplier<List<Integer>> supplier) {
        return Collector.of(
                supplier,
                (a, t) -> {
                    long val = a.size() > 0 ? Math.min(a.get(0),t) : t;
                    if (a.size() > 0) {
                        a.set(0, (int) val);
                    } else {
                        a.add((int) val);
                    }
                },
                (a, b) -> {
                    long bval = b.get(0);
                    long aval = Math.min(a.get(0),bval);
                    a.set(0, (int) aval);
                    return a;
                },
                a -> a, Collector.Characteristics.CONCURRENT);
    }

    public static Collector<Integer, List<Integer>, List<Integer>>
    maxInt(final Supplier<List<Integer>> supplier) {
        return Collector.of(
                supplier,
                (a, t) -> {
                    long val = a.size() > 0 ? Math.max(a.get(0),t) : t;
                    if (a.size() > 0) {
                        a.set(0, (int) val);
                    } else {
                        a.add((int) val);
                    }
                },
                (a, b) -> {
                    long bval = b.get(0);
                    long aval = Math.max(a.get(0),bval);
                    a.set(0, (int) aval);
                    return a;
                },
                a -> a, Collector.Characteristics.CONCURRENT);
    }

    public static Collector<Integer, List<Integer>, List<Integer>>
    countInt(final Supplier<List<Integer>> supplier) {
        return Collector.of(
                supplier,
                (a, t) -> {
                    long val = a.size() > 0 ? (a.get(0)+1) : 1;
                    if (a.size() > 0) {
                        a.set(0, (int) val);
                    } else {
                        a.add((int) val);
                    }
                },
                (a, b) -> {
                    long bval = b.get(0);
                    long aval = a.get(0) + bval;
                    a.set(0, (int) aval);
                    return a;
                },
                a -> a, Collector.Characteristics.CONCURRENT);
    }

    @Override
    public Supplier<List<Integer>> supplier() {
        return ArrayList::new;
    }

    @Override
    public BiConsumer<List<Integer>, Integer> accumulator() {
        return List::add;
    }

    @Override
    public BinaryOperator<List<Integer>> combiner() {
        return (list, list2) -> {
            list.addAll(list2);
            return list;
        };
    }

    @Override
    public Function<List<Integer>, List<Integer>> finisher() {
        return (list) -> list;
    }

    @Override
    public Set<Characteristics> characteristics() {
        return EnumSet.of
                (Characteristics.CONCURRENT);
    }


}
