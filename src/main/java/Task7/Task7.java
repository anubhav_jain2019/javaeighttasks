package Task7;


import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collector;

public class Task7 {

    //    small performance report of below code to run Collector

    //   normal = 4916746020 nanoseconds with IDENTITY_FINISH CHARACTERISTICS
    //   parallel = 4983772010 nanoseconds with IDENTITY_FINISH CHARACTERISTICS


    //   normal = 5006526833 nanoseconds with UNORDERED CHARACTERISTICS
    //   parallel = 4997269581 nanoseconds with UNORDERED CHARACTERISTICS


    //  normal = 4929929409 nanoseconds  with CONCURRENT CHARACTERISTICS
    //  parallel = 4926875969 nanoseconds  with CONCURRENT CHARACTERISTICS


    public static void main(String[] args) {

        long start = System.nanoTime();
        List<Integer> numbers = IntCollector.getIntStreamList().parallelStream().collect(new IntCollector());
//        numbers.forEach(System.out::println);

        Collector<Integer, List<Integer>, List<Integer>> sumCollector = IntCollector.summingInt(ArrayList::new);
        List<Integer> sum = numbers.stream().collect(sumCollector);
        System.out.println("Sum of Numbers = " +sum.get(0));

        Collector<Integer, List<Integer>, List<Integer>> minCollector = IntCollector.minInt(ArrayList::new);
        List<Integer> min = numbers.stream().collect(minCollector);
        System.out.println("Min of Numbers = " +min.get(0));

        Collector<Integer, List<Integer>, List<Integer>> maxCollector = IntCollector.maxInt(ArrayList::new);
        List<Integer> max = numbers.stream().collect(maxCollector);
        System.out.println("Max of Numbers = " +max.get(0));

        Collector<Integer, List<Integer>, List<Integer>> countCollector = IntCollector.countInt(ArrayList::new);
        List<Integer> count = numbers.stream().collect(countCollector);
        System.out.println("Count of Numbers = " +count.get(0));

        long end = System.nanoTime();
        System.out.println("Time taken to execute in nanoseconds = " + (end - start));
    }


}
