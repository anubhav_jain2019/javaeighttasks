package Task4;

@FunctionalInterface
public interface ThreeFunction {
    int calculate(int first, int second, int third);
}
