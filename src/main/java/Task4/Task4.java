package Task4;

public class Task4 {
    public static void main(String[] args) {
        ThreeFunction fmul = (int x, int y, int z) -> {
            return x * y * z;
        };

        ThreeFunction fsum = (int x, int y, int z) -> {
            return x + y + z;
        };

        System.out.println(fsum.calculate(5, 4, 3));
        System.out.println(fmul.calculate(3, 4, 2));
    }
}
