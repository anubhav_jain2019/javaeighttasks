package Task5;

import java.util.List;

public class Author {
    public Author(String name, short age, List books) {
        this.name = name;
        this.age = age;
        this.books = books;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public short getAge() {
        return age;
    }

    public void setAge(short age) {
        this.age = age;
    }

    public List getBooks() {
        return books;
    }

    public void setBooks(List books) {
        this.books = books;
    }

    String name;
    short age;
    List books;

    @Override
    public String toString() {
        return getName();
    }
}
