package Task5;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Task5 {
    public static void main(String[] args) {

        List<Book> books = new ArrayList<Book>();
        List<Author> authors = new ArrayList<Author>();
        List<Author> singleAuthor = new ArrayList<Author>();
        List<Author> otherAuthors = new ArrayList<Author>();
        Author author1 = new Author("ABC", (short) 20, books);
        Author author2 = new Author("DEF", (short) 21, books);
        Author author3 = new Author("GHI", (short) 22, books);
        Author author4 = new Author("JKL", (short) 23, books);

        authors.add(author1);
        authors.add(author2);
        authors.add(author3);
        authors.add(author4);

        singleAuthor.add(author1);
        otherAuthors.add(author3);
        otherAuthors.add(author4);

        books.add(new Book("Book1", authors, 130));
        books.add(new Book("Book2", authors, 330));
        books.add(new Book("Book3", singleAuthor, 240));
        books.add(new Book("Book4", books, 190));


        maxPageBook(books);
        System.out.println();

        minPageBook(books);
        System.out.println();

        anyBookMoreThanTwoCentsLength(books);
        System.out.println();

        allBooksMoreThanTwoCentsLength(books);
        System.out.println();

        filterBooksWithOneAuthor(books);
        System.out.println();

        System.out.println("List of All Book Titles -");
        books.forEach(System.out::println);
        System.out.println();

        authors.addAll(singleAuthor);
        authors.addAll(otherAuthors);
        distinctBooks(authors);
        System.out.println();

        filterDataUsingParallelStream(books);
        System.out.println();
    }

    public static void maxPageBook(List<Book> books){
        Optional<Book> maxPageBook = books.stream().max((book1, book2) -> {
            return book1.getNumberOfPages() - book2.getNumberOfPages();
        });
        System.out.println("Max Page Book : " + maxPageBook.get().getTitle());
    }

    public static void minPageBook(List<Book> books){
        Optional<Book> minPageBook = books.stream().min((book1, book2) -> {
            return book1.getNumberOfPages() - book2.getNumberOfPages();
        });
        System.out.println("Min Page Book : " + minPageBook.get().getTitle());
    }

    public static void anyBookMoreThanTwoCentsLength(List<Book> books){
        if (books.stream().anyMatch(new Predicate<Book>() {
            @Override
            public boolean test(Book book) {
                return book.getNumberOfPages() > 200;
            }
        })) {
            System.out.println("Book having more than 200 pages is available.");
        } else {
            System.out.println("No Book have more than 200 pages.");
        }
    }

    public static void allBooksMoreThanTwoCentsLength(List<Book> books){
        if (books.stream().allMatch(new Predicate<Book>() {
            @Override
            public boolean test(Book book) {
                return book.getNumberOfPages() > 200;
            }
        })) {
            System.out.println("All Books have more than 200 pages.");
        } else {
            System.out.println("Not all books have more than 200 pages.");
        }
    }

    public static void distinctBooks(List<Author> authors){
        List<Author> allDistinctAuthors = authors.stream().distinct().collect(Collectors.toList());
        System.out.println("Distinct Books -");
        Task3.Sandbox.printElements(Collections.singletonList(allDistinctAuthors));
    }


    public static void filterBooksWithOneAuthor(List<Book> books){
        List<Book> oneAuthorBooks= books.stream().filter(book -> book.getAuthors().size() == 1).collect(Collectors.toList());
        if (oneAuthorBooks.size() >= 1) {
            System.out.println("List of Books with One Author -");
            Task3.Sandbox.printElements(Collections.singletonList(oneAuthorBooks));
        } else {
            System.out.println("No Book has only One Author.");
        }
    }


    public static void filterDataUsingParallelStream(List<Book> books){
        List<Book> oneAuthorBooks= books.parallelStream().filter(book -> book.getAuthors().size() == 1).collect(Collectors.toList());
        if (oneAuthorBooks.size() >= 1) {
            System.out.println("List of Books with One Author -");
            Task3.Sandbox.printElements(Collections.singletonList(oneAuthorBooks));
        } else {
            System.out.println("No Book has only One Author.");
        }
    }
}
