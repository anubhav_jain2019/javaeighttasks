package Task6;

public class Employee {
        private String name;
        private int age;
        private String sex;
        private double salary;

    public String getName() {
        return name;
    }

    public Employee(Object name) {
        this.name = name.toString();
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return getName();
    }
}
