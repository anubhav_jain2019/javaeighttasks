package Task6;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

public class EmployeeCollector implements Collector<String,List<Employee>, List<Employee>> {

    @Override
    public Supplier<List<Employee>> supplier() {
        return ArrayList::new;
    }

    @Override
    public BiConsumer<List<Employee>, String> accumulator() {
       return (list, str) -> {
            Employee e = new Employee(str);
            list.add(e);
        };
    }

    @Override
    public BinaryOperator<List<Employee>> combiner() {
        return (list, list2) -> {
            list.addAll(list2);
            return list;
        };
    }

    @Override
    public Function<List<Employee>, List<Employee>> finisher() {
        return (list) -> list;
    }

    @Override
    public Set<Characteristics> characteristics() {
        return EnumSet.of
                (Characteristics.UNORDERED);
    }

}